﻿using Interpreter.Helpers;
using Interpreter.Interfaces;

namespace Interpreter.ViewModels
{
    public class InterpreterCommandViewModel : ViewModelBase, IInterpreterCommand
    {
        private ushort _first;
        private Operation.Code _operation;
        private ushort _second;
        private ushort _third;

        public InterpreterCommandViewModel()
        {
            _first = 0;
            _second = 0;
            _third = 0;
            _operation = 0;
        }

        public InterpreterCommandViewModel(IInterpreterCommand command)
        {
            _first = command.First;
            _second = command.Second;
            _third = command.Third;
            _operation = command.Operation;
        }

        public InterpreterCommandViewModel(string command)
        {
            var values = command.Split(',');
            First = !ushort.TryParse(values[0], out var first) ? (ushort) 0 : first;
            Second = !ushort.TryParse(values[1], out var second) ? (ushort) 0 : second;
            Third = !ushort.TryParse(values[2], out var third) ? (ushort) 0 : third;
            Operation = !ushort.TryParse(values[3], out var cmd) ? Operation = 0 : (Operation.Code) cmd;
        }

        public ushort First
        {
            get => _first;
            set => Change(ref _first, value);
        }

        public ushort Second
        {
            get => _second;
            set => Change(ref _second, value);
        }

        public ushort Third
        {
            get => _third;
            set => Change(ref _third, value);
        }

        public Operation.Code Operation
        {
            get => _operation;
            set => Change(ref _operation, value);
        }

        public override string ToString()
        {
            return $"{First}, {Second}, {Third}, {Helpers.Operation.Name[Operation]}";
        }
    }
}