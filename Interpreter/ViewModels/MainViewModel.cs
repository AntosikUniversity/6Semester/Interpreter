﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using Interpreter.Helpers;
using Microsoft.Win32;

namespace Interpreter.ViewModels
{
    public class MainViewModel
    {
        public MainViewModel()
        {
            Commands = new ObservableCollection<InterpreterCommandViewModel>();
            EditableCommand = new InterpreterCommandViewModel();

            RunCommand = new Command(RunInterpreterCommand);
            RunAndAddCommand = new Command(RunAndAddInterpreterCommand);
            OpenWorkspace = new Command(OpenFromFile);
            SaveWorkspace = new Command(SaveToFile);
        }

        public ObservableCollection<InterpreterCommandViewModel> Commands { get; set; }
        public InterpreterCommandViewModel EditableCommand { get; set; }

        public ICommand RunCommand { get; }
        public ICommand RunAndAddCommand { get; }
        public ICommand OpenWorkspace { get; }
        public ICommand SaveWorkspace { get; }

        private void SaveToFile()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Filter = "Text files (*.txt)|*.txt"
            };
            if (saveFileDialog.ShowDialog() == true)
                Model.Interpreter.SaveToFile(saveFileDialog.FileName, Commands);
        }

        private void OpenFromFile()
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = "Text files (*.txt)|*.txt"
            };
            if (openFileDialog.ShowDialog() == true)
            {
                Commands.Clear();
                var loadedCommands = Model.Interpreter.LoadFromFile(openFileDialog.FileName);
                foreach (var command in loadedCommands) Commands.Add(new InterpreterCommandViewModel(command));
            }
        }

        private void AddInterpreterCommand()
        {
            Commands.Add(new InterpreterCommandViewModel(EditableCommand));

            EditableCommand.First = 0;
            EditableCommand.Second = 0;
            EditableCommand.Third = 0;
            EditableCommand.Operation = 0;
        }

        private void RunInterpreterCommand()
        {
            try
            {
                switch (EditableCommand.Operation)
                {
                    case Operation.Code.PrintAllInSys:
                        Model.Interpreter.RunCommand(EditableCommand, s => MessageBox.Show(s));
                        break;
                    default:
                        Model.Interpreter.RunCommand(EditableCommand);
                        break;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        private void RunAndAddInterpreterCommand()
        {
            RunInterpreterCommand();
            AddInterpreterCommand();
        }
    }
}