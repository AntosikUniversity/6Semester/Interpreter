﻿using System.Collections.Generic;

namespace Interpreter.Helpers
{
    public static class Operation
    {
        public enum Code
        {
            PrintAllInSys = 0,
            NOT = 1,
            OR = 2,
            AND = 3,
            XOR = 4,
            Implication = 5,
            Coimplication = 6,
            Equivalence = 7,
            NOR = 8,
            NAND = 9,
            Sum = 10,
            Subtraction = 11,
            Multiply = 12,
            DIV = 13,
            MOD = 14,
            SWAP = 15,
            SetByte = 16,
            PrintFirstInSys = 17,
            EnterWithSystem = 18,
            Get2pNOD = 19,
            LeftShift = 20,
            RightShift = 21,
            CycleLeftShift = 22,
            CycleRightShift = 23,
            Copy = 24,
        }

        public static Dictionary<Code, string> Name = new Dictionary<Code, string>
        {
            {Code.PrintAllInSys, "PrintAllInSys"},
            {Code.NOT, "NOT"},
            {Code.OR, "OR"},
            {Code.AND, "AND"},
            {Code.XOR, "XOR"},
            {Code.Implication, "Implication"},
            {Code.Coimplication, "Coimplication"},
            {Code.Equivalence, "Equivalence"},
            {Code.NOR, "NOR"},
            {Code.NAND, "NAND"},
            {Code.Sum, "Sum"},
            {Code.Subtraction, "Subtraction"},
            {Code.Multiply, "Multiply"},
            {Code.DIV, "DIV"},
            {Code.MOD, "MOD"},
            {Code.SWAP, "SWAP"},
            {Code.SetByte, "SetByte"},
            {Code.PrintFirstInSys, "PrintFirstInSys"},
            {Code.EnterWithSystem, "EnterWithSystem"},
            {Code.Get2pNOD, "Get2pNOD"},
            {Code.LeftShift, "LeftShift"},
            {Code.RightShift, "RightShift"},
            {Code.CycleLeftShift, "CycleLeftShift"},
            {Code.CycleRightShift, "CycleRightShift"},
            {Code.Copy, "Copy"},
        };
    }
}