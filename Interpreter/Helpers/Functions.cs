﻿using System;

namespace Interpreter.Helpers
{
    public static class Functions
    {
        public static int HornerScheme(string number, int system)
        {
            var result = 0;

            foreach (var character in number)
            {
                var charNum = character - '0';
                result = result * system + (char.IsDigit(character) ? charNum : char.ToUpper(character) - 'A' + 10);
            }

            return result;
        }

        public static string InvertedHornerScheme(int number, int system)
        {
            var result = "";

            if (system == 0) return "";

            do
            {
                var mod = number % system;
                if (mod > 9)
                    result += Convert.ToChar('A' + mod - 10);
                else
                    result += mod;
                number = number / system;
            } while (number != 0);

            return result;
        }
    }
}