﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Interpreter.Annotations;
using Interpreter.Helpers;
using Interpreter.Interfaces;
using Microsoft.VisualBasic;

namespace Interpreter.Model
{
    public static class Interpreter
    {
        public static ushort MAX_NUMBER = (ushort) (Math.Pow(2, 9) - 1);
        public static uint MAX_NUMBER_32 = (uint) (Math.Pow(2, 32) - 1);

        public static IInterpreterCommand NumberToCommand(uint number)
        {
            if (number < 0 || number > MAX_NUMBER_32) throw new ArgumentOutOfRangeException($"Argument ${number} must be 32-bit unsigned!");

            var first = (ushort) (number >> (32 - 9)); // first 9bit
            var second = (ushort) ((number >> (32 - 18)) & ~(~0 << 9)); // second 9 bit
            var third = (ushort) ((number >> (32 - 27)) & ~(~0 << 9)); // third 9 bit
            var operation = (Operation.Code) (number & ~(~0 << 5)); // last 5 bit
            return new InterpreterCommand
            {
                First = first,
                Second = second,
                Third = third,
                Operation = operation
            };
        }

        public static uint CommandToNumber(IInterpreterCommand command)
        {
            var first = (uint) command.First << (32 - 9);
            var second = (uint) command.Second << (32 - 18);
            var third = (uint) command.Third << (32 - 27);
            var operation = (uint) command.Operation;

            return first | second | third | operation;
        }

        public static void RunCommand(IInterpreterCommand command, [CanBeNull] Action<string> after = null)
        {
            switch (command.Operation)
            {
                case Operation.Code.PrintAllInSys:
                {
                    after?.Invoke($"" +
                                  $"1st - {Functions.InvertedHornerScheme(command.First, command.First)} \n" +
                                  $"2nd - {Functions.InvertedHornerScheme(command.Second, command.First)} \n" +
                                  $"3rd - {Functions.InvertedHornerScheme(command.Third, command.First)} \n" +
                                  $"Operator - 0");
                }
                    break;
                case Operation.Code.NOT:
                {
                    command.Third = (ushort) (~command.First & MAX_NUMBER);
                    }
                    break;
                case Operation.Code.OR:
                {
                    command.Third = (ushort) (command.First | command.Second);
                }
                    break;
                case Operation.Code.AND:
                {
                    command.Third = (ushort) (command.First & command.Second);
                }
                    break;
                case Operation.Code.XOR:
                {
                    command.Third = (ushort) (command.First ^ command.Second);
                }
                    break;
                case Operation.Code.Implication:
                {
                    command.Third = (ushort) ((~command.First | command.Second) & MAX_NUMBER);
                }
                    break;
                case Operation.Code.Coimplication:
                {
                    command.Third = (ushort) ((~command.Second | command.First) & MAX_NUMBER);
                }
                    break;
                case Operation.Code.Equivalence:
                {
                    command.Third = (ushort) ((~command.First | command.Second) & (command.First | ~command.Second) &
                                              MAX_NUMBER);
                }
                    break;
                case Operation.Code.NOR:
                {
                    command.Third = (ushort) (~(command.First | command.Second) & MAX_NUMBER);
                }
                    break;
                case Operation.Code.NAND:
                {
                    command.Third = (ushort) (~(command.First & command.Second) & MAX_NUMBER);
                }
                    break;
                case Operation.Code.Sum:
                {
                    var sum = (ushort) (command.First + command.Second);
                    command.Third = sum > MAX_NUMBER ? (ushort) (sum - MAX_NUMBER) : sum;
                }
                    break;
                case Operation.Code.Subtraction:
                {
                    var sub = command.First - command.Second;
                    command.Third = (ushort) (sub < 0 ? MAX_NUMBER + sub : sub);
                }
                    break;
                case Operation.Code.Multiply:
                {
                    var multi = command.First * command.Second;
                    command.Third = (ushort) (multi % MAX_NUMBER);
                }
                    break;
                case Operation.Code.DIV:
                {
                    command.Third = (ushort) (command.First / command.Second);
                }
                    break;
                case Operation.Code.MOD:
                {
                    command.Third = (ushort) (command.First % command.Second);
                }
                    break;
                case Operation.Code.SWAP:
                {
                    var temp = command.First;
                    command.First = command.Second;
                    command.Second = temp;
                }
                    break;
                case Operation.Code.SetByte:
                {
                    var x = command.First;
                    var y = command.Second;
                    var p = command.Third;
                    const int n = 1;
                    command.First = (ushort) ((x & ~(~(~0 << n) << (p + 1 - n))) | ((y & ~(~0 << n)) << (p + 1 - n)));
                }
                    break;
                case Operation.Code.PrintFirstInSys:
                {
                    after?.Invoke(
                        $"1st - {Functions.InvertedHornerScheme(command.First, command.Second)} in {command.Second} system");
                }
                    break;
                case Operation.Code.EnterWithSystem:
                {
                    var input = Interaction.InputBox($"Enter number in ${command.Second} system", "Entering text", "");
                    command.First = (ushort) Functions.HornerScheme(input, command.Second);
                }
                    break;
                case Operation.Code.Get2pNOD:
                {
                    if (command.First == 0 || command.First % 2 != 0)
                    {
                        command.Third = 0;
                        return;
                    }

                    var result = 1;
                    var num = (int) command.First;
                    do
                    {
                        if (num % 2 != 0) break;
                        num = num / 2;
                        result *= 2;
                    } while (num != 0);

                    command.Third = (ushort) result;
                }
                    break;
                case Operation.Code.LeftShift:
                {
                    command.Third = (ushort) ((command.First << command.Second) & MAX_NUMBER);
                }
                    break;
                case Operation.Code.RightShift:
                {
                    command.Third = (ushort) ((command.First >> command.Second) & MAX_NUMBER);
                }
                    break;
                case Operation.Code.CycleLeftShift:
                {
                    command.Third = (ushort) ((command.First >> command.Second) |
                                              ((command.First << (9 - command.Second)) & MAX_NUMBER));
                }
                    break;
                case Operation.Code.CycleRightShift:
                {
                    command.Third = (ushort) ((command.First << command.Second) |
                                              ((command.First >> (9 - command.Second)) & MAX_NUMBER));
                }
                    break;
                case Operation.Code.Copy:
                {
                    command.First = command.Second;
                }
                    break;
            }
        }

        public static IEnumerable<IInterpreterCommand> LoadFromFile(string path)
        {
            var commands = new List<IInterpreterCommand>();

            using (var reader = new BinaryReader(File.Open(path, FileMode.Open), Encoding.ASCII))
            {
                var pos = 0;
                var length = (int) reader.BaseStream.Length;

                while (pos < length)
                {
                    var command = reader.ReadUInt32();
                    commands.Add(NumberToCommand(command));

                    pos += sizeof(uint);
                }
            }

            return commands;
        }

        public static void SaveToFile(string path, IEnumerable<IInterpreterCommand> commands)
        {
            using (var writer = new BinaryWriter(File.Open(path, FileMode.Create), Encoding.ASCII))
            {
                foreach (var command in commands)
                    writer.Write(CommandToNumber(command));
            }
        }
    }
}