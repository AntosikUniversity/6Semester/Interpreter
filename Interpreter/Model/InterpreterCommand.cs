﻿using Interpreter.Helpers;
using Interpreter.Interfaces;

namespace Interpreter.Model
{
    public class InterpreterCommand : IInterpreterCommand
    {
        public InterpreterCommand()
        {
            First = 0;
            Second = 0;
            Third = 0;
            Operation = 0;
        }

        public ushort First { get; set; }

        public ushort Second { get; set; }

        public ushort Third { get; set; }

        public Operation.Code Operation { get; set; }

        public override string ToString()
        {
            return $"{First}, {Second}, {Third}, {Helpers.Operation.Name[Operation]}";
        }
    }
}