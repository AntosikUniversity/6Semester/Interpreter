﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Interpreter.ViewModels;

namespace Interpreter.Behaviors
{
    public static class CommandsToString
    {
        public static readonly DependencyProperty CollectionProperty =
            DependencyProperty.RegisterAttached("Collection", typeof(ObservableCollection<InterpreterCommandViewModel>),
                typeof(CommandsToString),
                new PropertyMetadata(null, (d, e) =>
                {
                    var textBlock = d as TextBox;
                    var collection = e.NewValue as ObservableCollection<InterpreterCommandViewModel>;
                    collection.CollectionChanged += (s, a) =>
                    {
                        textBlock.Text = string.Join("\n", collection.Select(command => command.ToString()));
                    };
                }));

        public static ObservableCollection<InterpreterCommandViewModel> GetCollection(DependencyObject obj)
        {
            return (ObservableCollection<InterpreterCommandViewModel>) obj.GetValue(CollectionProperty);
        }

        public static void SetCollection(DependencyObject obj, ObservableCollection<InterpreterCommandViewModel> value)
        {
            obj.SetValue(CollectionProperty, value);
        }
    }
}