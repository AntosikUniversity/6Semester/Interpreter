﻿using Interpreter.Helpers;

namespace Interpreter.Interfaces
{
    public interface IInterpreterCommand
    {
        ushort First { get; set; }
        ushort Second { get; set; }
        ushort Third { get; set; }
        Operation.Code Operation { get; set; }

        string ToString();
    }
}