﻿using System.Windows.Controls;

namespace Interpreter.Controls
{
    /// <summary>
    ///     Interaction logic for Visual.xaml
    /// </summary>
    public partial class Visual : UserControl
    {
        public Visual()
        {
            InitializeComponent();
        }
    }
}